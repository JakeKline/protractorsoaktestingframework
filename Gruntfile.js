module.exports = function(grunt){
    require('load-grunt-tasks')(grunt);
    grunt.initConfig({
        clean: {
            soakTemp: '<%= project.test %>/soakTemp/**/*'
        },
        protractor: {
            options: {
                configFile: '', // Default config file
                keepAlive: true, // If false, the grunt process stops when the test fails.
                noColor: false, // If true, protractor will not use colors in its output.
                args: {
                    troubleshoot:true
                }
            },
            all: {
                options: {
                    configFile: 'test/referenceConf.js', // Target-specific config file
                    args: {
                        troubleshoot:true,
                        debug:true
                    }
                }
            },
            demo: {
                options: {
                    configFile: 'test/buySoakWithPluginsConf.js', // Target-specific config file
                    args: {

                    }
                }
            }
        }
    });

    grunt.registerTask('soak',
        'protractor:demo'
    )
};