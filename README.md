# Soak testing using Protractor

## To run a soak test on a local machine using protractor, you will need:
1.	The node project in question;
2.	The amount of time the test will let the application 'soak,' (e.g. 24 hours, 48 hours, 2 weeks,)
3.	A set of e2e tests that represent typical actions against the application under test throughout the duration the application soaks & an approximate run-time of the set, e.g. 1 minute, ten minutes, that will be used to determine the total number of tests (the set of all sets) needed to equal the target time (for this project 9000 tests came in at an approximate run time of 24 hours)
4.	The rate at which individual tests, or discrete sets of these tests will be run during the overall test run (e.g. 3 tests per minute, 4 tests per hour)
5.	A soak-test specific protractor conf file configured to the specifications below
6.	A dedicated machine that can handle allocation at least 4 gigs of RAM to the node process that will run the tests
7.	Node.js installed globally on this machine
8.	The grunt-cli node package installed globally on this machine
9.	Access to the command line on this machine


## How to run the soak test (in the integration environment):
1.	First, run any dependencies specific to your project or mocking frameworks.
2.	Open a new command shell and navigate to the location that houses the application's grunt file, in this case ../kiosk-ui/ui
3.  Execute an npm install command here to install required node packages, e.g. "npm install"
4.	Execute the following command in the shell, "node --max_old_space_size=4096 path/to/globally/installed/grunt-cli/npm/node_modules/grunt-cli/bin/grunt soak"
  -	More about max_old_space_size - it's a node flag to indicate the amount of memory in megabytes the node process that runs the grunt task may consume.  For x64 machines, node's default limit is 1.4gb.  During testing of the configuration this gave us about an hour and a half of reliable testing (2 to 3,000 tests) before throwing a fatal out-of-memory exception.  For a 24-hour period it is recommended that the flag be set to at least 4096mb (4gb) or higher.  My personal recommendation is to use a machine with at least 16GB of ram and set the flag to 7196mb.
  -	The command as executed from Jake's laptop: "node --max_old_space_size=4096 C:/Users/jake.kline/AppData/Roaming/npm/node_modules/grunt-cli/bin/grunt e2e:soak:integration"
  -	More about the soak param -
    -	For this shell a simple call to the soak testing task in the Grunt file.  Customize this task as needed.
8.	For the project for which this was initially developed, ~9000 tests were generated and executed against the running application in one instance of the chrome browser.  The tests will run for around 24 hours, capture screenshots of failing tests, gather test performance metrics viewable in a generated html page, issue an html test results page, and report console errors issued by the Chrome.

## Current Protractor Configuration Expressed in Goal(s), Problems, and Resolutions
 Goal: Run several thousand tests in a single (browser, application) instance whose total run time matches or is as close as possible to the overall soak goal.

 **Problem 0**: I want all tests executed in one browser instance.

 _Resolution_: set maxSessions: 1; Also, if testing in chrome, recommend setting directConnect:true

 **Problem 1**: Protractor's spec and suite features allow users to either specify test-name patterns to be matched by protractor as a set of tests (spec) or allow users to specify by name a set of tests to be run (suite.)  In the case of specs, matching via regex results in a set of the unique matches making repeating test names within the set improbable.  In the case of suites, it would be inconvenient and time consuming for a human to individually specify thousands of test names in the protractor conf or associated reference file, not to mention making the files themselves cumbersome and unwieldy to maintain.

 _Resolution_: Created a temporary folder that houses test files programmatically generated before run time.  Configured the "specs:" portion of the conf to retrieve all files from this temp folder, e.g. specs:'./test/soakTemp/*.js'

 **Problem 2**: Now the protractor soak test looks to an empty folder for tests

 _Resolution_: Created a folder in the e2e tests file structure whose sole purpose is to house the e2e tests that will be performed by the soak test (essentially duplicates of existing tests.)  Targeted the beforeLaunch method to initiate several setup steps, since this method is executed before the global protractor object that runs the tests is instantiated.  The following tasks are performed by the code blocks: 1) check to make sure the soakTemp folder exists & if not create it; 2) read file names from the soak-specific e2e folder into an array; 3) read the contents of the global beforeEach and afterEach methods employed by the "meter"; 4) For the specified number of times (number of tests to be run) loop through the array of file names, prefix a number of the file name to make it unique to all other soak files to be published in the temp dir, and copy the new file into the soakTemp directory. 5) Like 4, but for every n-number of e2e tests, add a "walk away" test to simulate a user leaving the kiosk.

 **Problem 3**: Now I have 9000 tests in a temporary directory that i may not necessarily want to run again or run again int he same order

 _Resolution_: In the onCleanup method, included a code block that deletes everything from the soakTemp directory

 **Problem 4**: Well, the soak test ran but crashed and I still have a ton of files in that soakTemp directory

 _Resoltuions_: Run grunt clean:<target folder> (not implemented)

 **Problem 5**: Great, but the soak test still crashed with a fatal memory allocation error

 _Resolution_: Via command line, run the grunt-cli grunt file using node passing in the normal soak testing arguments (see 6. under "How to run the Buy Kiosk-ui soak...")

 **Problem 6**: Soaking implies that tests should run at human speed based on a vetted transactions-per-minute.  The rate at which each test is executed against the browser and application needs to emulate human usage where in a typical protractor run the test steps are executed at a much higher pace than humanly possible

 _Resolution_: Leverage protractor's built in "global variables", arbitrary, custom variables accessible to every test stored under "browser.params".  These can configured and accessed anywhere in the tests, in the protractor onPrepare method, or in the params JSON structure (the latter two found in the protractor conf.)  For our purposes we created them in the conf as such:
	````  params: {
         meter: {
      transactionCount:0,
      lastStartTime:0
                },
       testingCommenced:0
       }````

Furthermore, in the beforeLaunch method, we added one meter.js file to the soakTemp folder that contains only a jasmine beforeEach and afterEach method (also global to the test run.)  The bforeEach method sets the testingCommenced flag to true at the beginning of the overall test run and increments the transactionCount variable by one after every test.  If it is the first test to be run or is the fourth test (meaning three tests have run), the routine sets the lastStartTime variable to the current time.  Finally if it is the first test the routine launches the Chrome browser instance.

Finally, after each test, in the afterEach method the routine checks to see if a set of three tests has been run and if so records the time.  If the difference between the stored lastStartTime and the newly registered time is under a minute, the browser waits the remainder of the minute.

**Problem 7**: Great, the soak test runs for 24 hours.  How do we analyze the results?

_Resolution_: A combination of three mechanisms provides insight into the test run.  First, the HtmlScreenshotReporter logs screenshots (configured to capture the screen for failed tests giving a visual representation of the browser at the time the test failed) and a human-readable html page (soak-test-report.html) of the test results to an output folder (performance/screenshots) .  Given that tests are sometimes unreliable, this mechanism should be used in conjunction with the protractor-console-plugin which outputs to the console warnings and errors thrown by the browser and fails testing if an internal chrome error was thrown.  Chrome errors generally mean something went wrong with the browser which is what we're looking for.  Finally, there is a protractor-timeline-plugin that instruments the test effort.  Its assets are saved to the performance folder and include an index.html page and timeline.json file.  To view the instrumentation with IntelliJ, right-click on the index.html file, select "Open in browser", and select Chrome. NOTE: I've also included 'enableTimeline' in the chromeOptions section of 'capabilities' in the protractor conf.


