/**
 * Created by jake.kline on 11/10/2015.
 */
var jasmineReporters = require('jasmine-reporters');
var HtmlScreenshotReporter = require('protractor-jasmine2-screenshot-reporter');
var fs = require('fs');

function copyRandomFileToTempDirWithUniqueName (possibleFiles, possibleFilesDir, i) {
    var randomFile = possibleFiles[chance.natural({min: 0, max: possibleFiles.length - 1})];
    var fileContents = fs.readFileSync(possibleFilesDir + '/' + randomFile);
    fs.writeFileSync(soakTempDir + '/' + i + randomFile, fileContents);
}

var Chance = require('chance'),
    chance = new Chance();

var rimraf = require('rimraf');

var meterLocale = './test/meter.js';
var soakTestsDir = './test/e2eTests';
var walkDir = './test/walkAwayTests';
var soakTempDir = './test/soakTemp';

exports.config = {


    chromeDriver:'C:/AppPaths/chromedriver.exe',

    //must grab all from soakTemp dir
    specs:[ './soakTemp/*.js'],

    directConnect:true,

    exclude: [],

    capabilities: {
        browserName: 'chrome',
        chromeOptions: {
            'args': ['start-maximized', 'enable-crash-reporter-for-testing','enableTimeline']
        }
    },

    //keep it all in one browser
    maxSessions: 1,

    //browser.params for the meter
    params: {
        meter: {
            transactionCount:0,
            lastStartTime:0
        },
        testingCommenced:0
    },
    //rootElement: 'body'

    allScriptsTimeout: (1000*30*60),

    getPageTimeout: 10000,

    beforeLaunch: function() {
        if (fs.statSync(soakTempDir).isDirectory()) {
            rimraf.sync(soakTempDir+'/**/*');
        }

        var meterGlobal = fs.readFileSync(meterLocale);
        fs.writeFileSync(soakTempDir+'/'+'meter.js',meterGlobal);
        //non-recursive list of files
        var testFiles = fs.readdirSync(soakTestsDir);
        var walkFiles = fs.readdirSync(walkDir);

        var jobToRun = 1000; //ten thousand tests //FIXME
        for(var i=0; i<jobToRun; i++) {

            //for every thousandth transaction push a walkaway test into the mix
            if(i%1000===0)
            {
                copyRandomFileToTempDirWithUniqueName(walkFiles, walkDir, i);
            }
            else
            {
                copyRandomFileToTempDirWithUniqueName(testFiles, soakTestsDir, i);
            }
        }
    },

    onPrepare: function() {
        browser.getProcessedConfig().then(function(config) {
            console.log('Running with the following capabilities:', config.capabilities);
        });


        jasmine.getEnv().addReporter(
            new jasmineReporters.JUnitXmlReporter({
                savePath: 'test',
                filePrefix: 'e2e',
                consolidateAll: true
            })
        );
    },

    // A callback function called once tests are finished.
    onComplete: function() {

    },

    // A callback function called once the tests have finished running and
    // the WebDriver instance has been shut down. It is passed the exit code
    // (0 if the tests passed). This is called once per capability.
    onCleanUp: function(exitCode) {
        var testFiles = fs.readdirSync(soakTempDir);
        var jobToRun = testFiles.length;
        for(var i=0; i<jobToRun; i++) {
            fs.unlinkSync(soakTempDir+'/'+testFiles[i]);
        }

    },

    // A callback function called once all tests have finished running and
    // the WebDriver instance has been shut down. It is passed the exit code
    // (0 if the tests passed). This is called only once before the program
    // exits (after onCleanUp).
    afterLaunch: function() {

    },

    resultJsonOutputFile: 'soakTestOutputPlugin.json',

    restartBrowserBetweenTests: false,

    untrackOutstandingTimeouts: false,


    framework: "jasmine2",

    plugins: [{
        package: 'protractor-timeline-plugin',

        // Output json and html will go in this folder.
        outdir: 'performance'

        // Optional - if sauceUser and sauceKey are specified, logs from
        // SauceLabs will also be parsed after test invocation.
        //sauceUser: 'eosproject',
        //sauceKey: '6180c499-7396-42b8-bb4d-3614b967c42f'
    },
    {
        package: 'protractor-console-plugin',
        failOnWarning: false,
        failOnError:false,
        logWarnings:true,
        exclude: []
    },
    {
        chromeA11YDevTools: {
            treatWarningsAsFailures: false
        },
        package: 'protractor-accessibility-plugin'
    }
    ]
};
