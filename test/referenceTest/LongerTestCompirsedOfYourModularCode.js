/**
 * Created by jake.kline on 11/17/2015.
 */
describe('a set of \"it blocks\" that would include an entire flow, from clicking welcome screen to paying, analogous to Jeff\'s test cases ', function() {
  beforeEach(function(){
    //some meter logic here
  });
  afterEach(function(){
    //some meter logic here
  });
  it('should execute the first step in the case', function() {

  });
  it('should execute the second step in the case', function() {

  });
  it('should execute the third step in the case', function() {

  });
  it('should execute the fourth step in the case', function() {

  });
  it('should execute the fifth step in the case', function() {

  });
  it('should execute the nth step in the case', function() {

  });
});

/*if we had eight or nine long-running scripts like this saved in the
soak-specific folder the rate mechanism i'm talking about in the beforeLaunch
method of the protractor conf could copy some-thousand of the tests that end successfully
and some hundred/ten.whatever of the cases where people walk away from the
kiosk or other edge cases.*/

/*since i'm leveraging the spec globs rather than the suites to try keeping global vars intact for
the timer that checks if our tests completed too quickly for the 3/min or 2/min, it shouldn't matter too much
which tests get pulled in
*/
