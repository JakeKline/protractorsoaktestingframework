/**
 * Created by jake.kline on 11/6/2015.
 */
function soakMockPageObjects(){
    this.GoogleUrl = "http://www.google.com";
    this.SlalomUrl = "http://www.slalom.com";
}

module.exports = function(){
    return soakMockPageObjects();
}