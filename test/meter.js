/**
 * Created by jake.kline on 11/20/2015.
 */

var moment = require('moment');

jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;

beforeEach(function(){

  var firstQuestion = browser.params.testingCommenced;
  if(firstQuestion===false){
    browser.params.testingCommenced=true;
  }
  //increment the transaction count
  browser.params.meter.transactionCount+=1;

  var transactionCount = browser.params.meter.transactionCount;
  //register the test\'s start time if it is the first test to run or if it is s the first test
  //after a set of 3
  if(transactionCount===1||transactionCount%4===0)
  {
    browser.params.meter.lastStartTime=moment();
  }

  if (!pageLoaded) {
    welcomePage.get();
    pageLoaded = true;
  }
});
afterEach(function(){
  var transactions = browser.params.meter.transactionCount;
  //check for time remaining after 3 runs
  if(transactions%3===0){

    var startingTime = browser.params.meter.lastStartTime;


    var rate =60*1000;


    var wait = (moment()-startingTime);

    if(wait<rate) {
      browser.sleep((rate-wait));
    }
  }
});
