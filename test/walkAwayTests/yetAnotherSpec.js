/**
 * Created by jake.kline on 11/16/2015.
 */
describe('example test for soak Mock',function(){
    beforeEach(function(){
        browser.ignoreSynchronization = true;
    });
    it('should go to WaPo',function(){
        browser.get('http://www.washingtonpost.com');
        browser.sleep(10000);
        expect(browser.getTitle()).toContain('Washington');
    });
    browser.sleep(1000);
    it('should go to The Economist\'s official website',function(){

        browser.get('http://www.economist.com//');
        browser.sleep(1000);
        expect(browser.getTitle()).toEqual('The Economist - World News, Politics, Economics, Business & Finance');
    });
});