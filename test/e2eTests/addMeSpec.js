/**
 * Created by jake.kline on 11/10/2015.
 */
describe('example test for soak Mock',function(){
    beforeEach(function(){
        browser.ignoreSynchronization = true;
    });
    afterEach(function(){

    });
    it('should open a browser and go to the protractor home page',function(){
        console.log(protractor.config);
        browser.get('http://www.protractortest.org');
        browser.sleep(10000);
        expect(browser.getTitle()).toEqual('Jake\'s Soak Test');
    });
    it('should go to the jasmine bootjs page in the same chrome window',function(){

        browser.get('http://jasmine.github.io/2.0/boot.html');
        browser.sleep(1000);
        expect(browser.getTitle()).toEqual('boot.js');
    });
});