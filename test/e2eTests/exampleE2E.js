/** Created by jake.kline on 11/4/2015.*/
describe('example test for soak Mock',function(){
    beforeEach(function(){
        browser.ignoreSynchronization = true;
    });
    it('should open a browser and go to google',function(){
        browser.get('http://www.google.com');
        browser.sleep(10000);
        expect(browser.getTitle()).toEqual('Jake\'s Soak Test');
    });
    browser.sleep(1000);
    it('should go to Slalom in the same chrome window',function(){

        browser.get('http://www.slalom.com');
        browser.sleep(1000);
        expect(browser.getTitle()).toEqual('Slalom Consulting');
    });
});
